import React, { useState } from 'react'
import { Select } from 'antd'
import { eventData } from 'data/eventData'
import CardNews from 'components/Cards/CardNews'
import PaginationComp from 'components/Pagination/Pagination'
import BreadCrumb from 'components/BreadCrumbs/BreadCrumbs'

const { Option } = Select
const itemsPerPage = 8

export default function Event() {
  const [currentPage, setCurrentPage] = useState(1)
  const totalPages = Math.ceil(eventData.length / itemsPerPage)

  const startIndex = (currentPage - 1) * itemsPerPage
  const endIndex = startIndex + itemsPerPage

  const dataEvent = eventData.slice(startIndex, endIndex)

  const goToPage = (page) => {
    setCurrentPage(page)
  }

  const breadcrumbItems = [
    // { text: 'Home', link: '/' },
    { text: 'Event' },
  ]

  const handleSortChange = (value) => {
    // sorting logic
  }

  return (
    <div>
      <img
        src="./static/images/vectors/Ellipse.svg"
        alt=""
        className="absolute top-[100px] -z-10"
      />
      <div className="w:full px-[80px] mt-[72px]">
        <div className="flex items-center justify-between px-[73px]">
          <BreadCrumb items={breadcrumbItems} />
          <div>
            <Select
              placeholder="Urutkan"
              style={{ width: 260 }}
              onChange={handleSortChange}
            >
              <Option value="option1">Option 1</Option>
              <Option value="option2">Option 2</Option>
              <Option value="option3">Option 3</Option>
            </Select>
          </div>
        </div>
        <div className="px-[80px] w-full m-auto">
          <div className="flex flex-wrap gap-8 ml-[37px] mt-7">
            {dataEvent.map((e, index) => (
              <div key={index} className="w-[302px] mb-10 mr-4 md:mr-0">
                <CardNews
                  image={e.img}
                  title={e.title}
                  category={e.category}
                  date={e.date}
                  size="w-[302px] mb-10"
                />
              </div>
            ))}
          </div>

          <div className="flex justify-center mt-5 mb-[124px]">
            <PaginationComp
              currentPage={currentPage}
              totalPages={totalPages}
              onPageChange={goToPage}
              itemsPerPage={itemsPerPage}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
