import React, { useState } from 'react'
import {
  PhoneOutlined,
  MailOutlined,
  SendOutlined,
  ArrowRightOutlined,
} from '@ant-design/icons'
import InputCommon from 'components/Input/InputCommon'
import InputSelect from 'components/Input/InputSelect'
import { MaterialCard, BudgetCard } from 'components/Cards/Card'
import InputSubmit from 'components/Input/InputSubmit'
import { Form } from 'antd'
import Breadcrumbs from 'components/BreadCrumbs/BreadCrumbs'

export default function ContactUs() {
  const [currentStep, setCurrentStep] = useState(1)
  const [formValues, setFormValues] = useState({
    nama: '',
    email: '',
    phone: '',
  })

  const handleInputChange = (name, value) => {
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }))
  }

  const handleNextPageClick = () => {
    if (currentStep === 1) {
      if (!formValues.nama || !formValues.email || !formValues.phone) {
        return
      }
      setCurrentStep(2)
    } else {
      // eslint-disable-next-line no-console
      console.log()
    }
  }
  const breadcrumbItems = [
    // {
    //   // text: 'Home',
    //   // link: '/',
    // },
    { text: 'Contact Us' },
  ]

  const options = [
    {
      value: 'option1',
      label: 'Option 1',
    },
    {
      value: 'option2',
      label: 'Option 2',
    },
    {
      value: 'option3',
      label: 'Option 3',
    },
  ]

  const material = [
    {
      value: 'Idea',
      img: './static/images/vectors/vector-img-contect.svg',
    },
    {
      value: 'UI/UX',
      img: './static/images/vectors/vector-img-contect.svg',
    },
    {
      value: 'Spesification',
      img: './static/images/vectors/vector-img-contect.svg',
    },
    {
      value: 'Code',
      img: './static/images/vectors/vector-img-contect.svg',
    },
  ]

  const budget = [
    {
      value: '50 - 200 jt',
    },
    {
      value: '50 - 200 jt',
    },
    {
      value: '50 - 200 jt',
    },
    {
      value: '50 - 200 jt',
    },
  ]

  return (
    <div>
      <img
        src="./static/images/vectors/Ellipse.svg"
        alt=""
        className="absolute top-[100px]"
      />
      <div className="w-[627px] mt-[60px] m-auto ">
        <Breadcrumbs items={breadcrumbItems} />
        <div className="text">
          <h1 className="text-[36px] mb-4">Contact Us</h1>
          <p className="leading-7 font-normal">
            If you are interested in working with us or just want to talk shop
            with some really smart, talented people, let us know! Fill out the
            form and one of our team members will get back to you right away.
          </p>
          <div>
            {currentStep === 1 && (
              <div>
                <div>
                  <p className="font-semibold my-3">
                    PT. SOLUSI TEKNOLOGI NUSANTARA
                  </p>
                  <p className="leading-7 font-normal">
                    Komp. Ruko Aldiron Hero Bhakti JL. Daan Mogot 2 Blok D
                    No.8A, RT. 006, RW. 005, Duri Kepa, Kebon Jeruk, Kota
                    Jakarta Barat, DKI Jakarta. 11510
                  </p>
                  <div className="flex items-center mb-3">
                    <div className="flex items-center gap-2">
                      <PhoneOutlined
                        style={{ color: 'red', transform: 'rotate(90deg)' }}
                      />
                      <p className="m-0">081214136860</p>
                    </div>
                    <div className="flex items-center gap-2 ml-4">
                      <MailOutlined style={{ color: 'red' }} />
                      <p className="m-0">hello@nusantech.co</p>
                    </div>
                  </div>
                </div>
                <div className="flex items-center">
                  <div className="flex-grow line h-[1px] bg-[#E4E4E8]" />
                  <p className="mx-4 pt-4 text-[#A1A5B7]">Step 1 of 2</p>
                  <div className="flex-grow line h-[1px] bg-[#E4E4E8]" />
                </div>
                <div className="form">
                  <Form>
                    <div className="mt-[29px]">
                      <h1 className="text-[20px] font-semibold">
                        How we can contact you back?
                      </h1>{' '}
                      <InputCommon
                        type="text"
                        name="name"
                        required
                        label="Name"
                        placeholder="Please provide your name"
                        className="w-[full] h-[40px] rounded-lg"
                        messages="Name has not been filled in yet"
                        value={formValues.nama}
                        onChange={(e) =>
                          handleInputChange('nama', e.target.value)
                        }
                      />
                      <div className="flex gap-6">
                        <InputCommon
                          type="text"
                          name="email"
                          required
                          label="Email Address"
                          placeholder="Please enter your email address"
                          className="w-[302px] h-[40px] rounded-lg"
                          messages="The email address is not filled in yet"
                          value={formValues.email}
                          onChange={(e) =>
                            handleInputChange('email', e.target.value)
                          }
                        />
                        <InputCommon
                          type="text"
                          name="phone"
                          required
                          label="Phone Number"
                          placeholder="Please enter your phone number"
                          className="w-[302px] h-[40px] rounded-lg"
                          messages="The phone number is not filled in yet"
                          value={formValues.phone}
                          onChange={(e) =>
                            handleInputChange('phone', e.target.value)
                          }
                        />
                      </div>
                      <h1 className="mb-[-1px] text-[20px] font-semibold">
                        What we can do for you?
                      </h1>
                      <InputSelect
                        placeholder="Select Services"
                        messages="Select Services is not filled in yet"
                        options={options}
                        className="w-[full] rounded-lg text-[#626687]  focus:outline-none focus:border-blue-500 "
                        onChange={(value) =>
                          handleInputChange('provinsi', value)
                        }
                        label={undefined}
                        required={undefined}
                      />
                    </div>
                    <div>
                      <h1 className="text-[20px] font-semibold">
                        You already have
                      </h1>
                      <MaterialCard material={material} />
                    </div>
                    <div className="mt-[36px]">
                      <h1 className="mb-[24px] text-[20px] font-semibold">
                        Your Budget
                      </h1>
                      <p>
                        <BudgetCard budget={budget} />
                      </p>
                    </div>
                    <div className="mt-[60px] flex justify-end">
                      <InputSubmit
                        value="Next Page"
                        className="item px-[24px] text-center text-[#fff] bg-[#DF1E36] font-[600] h-[54px] mt-[10px] mb-[120px] rounded-lg cursor-pointer"
                        icon={<ArrowRightOutlined />}
                        onClick={handleNextPageClick}
                      />
                    </div>
                  </Form>
                </div>
              </div>
            )}
            {currentStep === 2 && (
              <div>
                <div className="flex items-center mb-2">
                  <div className="flex-grow line h-[1px] bg-[#E4E4E8]" />
                  <p className="mx-4 pt-4 text-[#A1A5B7]">Step 2 of 2</p>
                  <div className="flex-grow line h-[1px] bg-[#E4E4E8]" />
                </div>
                <div>
                  <p className="text-[20px] font-semibold">
                    Final step! Let's wrap this up.
                  </p>
                  <Form>
                    <InputCommon
                      type="text"
                      name="company_name"
                      required
                      label="Company Name"
                      placeholder="Please enter your company name"
                      className="w-[full] h-[40px] rounded-lg"
                      messages="The email address is not filled in yet"
                      value={formValues.nama}
                      onChange={(e) =>
                        handleInputChange('nama', e.target.value)
                      }
                    />
                    <InputSelect
                      placeholder="Select One"
                      messages="Anda belum memilih provinsi"
                      options={options}
                      label="Where did you find out about Nusantech"
                      required
                      className="w-[full] rounded-lg text-[#626687]  focus:outline-none focus:border-blue-500 "
                      onChange={(value) => handleInputChange('provinsi', value)}
                    />
                    <InputCommon
                      type="textarea"
                      name="message"
                      required
                      label="Your Message"
                      placeholder="write your message here"
                      className="w-[full] pb-[100px] rounded-lg"
                      messages="The message is not filled in yet"
                      value={formValues.nama}
                      onChange={(e) =>
                        handleInputChange('nama', e.target.value)
                      }
                    />
                    <div className="mt-[60px] flex justify-end">
                      <InputSubmit
                        value="Send Message"
                        className="item px-[24px] text-center text-[#fff] bg-[#DF1E36] font-[600] h-[54px] mt-[10px] mb-[120px] rounded-lg cursor-pointer"
                        icon={
                          <SendOutlined
                            style={{ transform: 'rotate(-45deg)' }}
                          />
                        }
                      />
                    </div>
                  </Form>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
